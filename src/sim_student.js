/* Hebbian Learner - robot and GUI script.
 *
 * Copyright 2016 Harmen de Weerd
 * Copyright 2017 Johannes Keyser, James Cooke
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const BOX_SIZE = 20;


simInfo = {
  maxSteps: 20000, // maximal number of simulation steps to run
  airDrag: 0.1,  // "air" friction of enviroment; 0 is vacuum, 0.9 is molasses
  boxFric: 0.0005, //
  boxMass: 0.1, //0.01,  // mass of boxes
  boxSize: BOX_SIZE,  // size of the boxes, in pixels
  robotSize: 2*7,  // robot radius, in pixels
  robotMass: 0.4, // robot mass (a.u)
  gravity: 0,  // constant acceleration in Y-direction
  bayRobot: null,  // currently selected robot
  baySensor: null,  // currently selected sensor
  bayScale: 3,  // scale within 2nd, inset canvas showing robot in it's "bay"
  doContinue: true,  // whether to continue simulation, set in HTML
  debugSensors: true,  // plot sensor rays and mark detected objects
  debugMouse: true,  // allow dragging any object with the mouse
  engine: null,  // MatterJS 2D physics engine
  world: null,  // world object (composite of all objects in MatterJS engine)
  runner: null,  // object for running MatterJS engine
  height: null,  // set in HTML file; height of arena (world canvas), in pixels
  width: null,  // set in HTML file; width of arena (world canvas), in pixels
  curSteps: 0, // increased by simStep()
  // custom values
  defaultForce: 0.0002,
  maxTorque: 0.004,
  learningRate: 0.01,
  forgettingRate: 0.005,
  activation_threshold: 0.5, // theta
  maxWeightInit: 0.1,      // v
  maxValProximitySensor: 50,
  maxValCollisionSensor: 5,
  maxClusterDistance: BOX_SIZE
};

// Description of robot(s), and attached sensor(s) used by InstantiateRobot()
RobotInfo = [
  {body: null,                       // for MatterJS body, added by InstantiateRobot()
    color: "red",                    // color of the robot marker
    init: {x: 50, y: 50, angle: .25 * Math.PI},  // initial position and orientation
    sensors: [                       // define an array of sensors on the robot
      // define left proximity sensor
      // {
      //   sense: senseDistance,    // function handle, determines type of sensor
      //   minVal: 0,               // minimum detectable distance, in pixels
      //   maxVal: simInfo.maxValProximitySensor, // maximum detectable distance, in pixels
      //   attachAngle: -Math.PI/2, // where the sensor is mounted on robot body
      //   lookAngle: 0,            // direction the sensor is looking (relative to center-out)
      //   id: "proximityLeft",     // a unique, arbitrary ID of the sensor, for printing/debugging
      //   parent: null,            // robot object the sensor is attached to, added by InstantiateRobot
      //   value: null,             // sensor value, i.e. distance in pixels; updated by sense() function
      // },
      // define front-left proximity sensor
      {
        sense: senseDistance,    // function handle, determines type of sensor
        minVal: 0,               // minimum detectable distance, in pixels
        maxVal: simInfo.maxValProximitySensor, // maximum detectable distance, in pixels
        attachAngle: -Math.PI/4, // where the sensor is mounted on robot body
        lookAngle: 0,            // direction the sensor is looking (relative to center-out)
        id: "proximityFrontLeft",     // a unique, arbitrary ID of the sensor, for printing/debugging
        parent: null,            // robot object the sensor is attached to, added by InstantiateRobot
        value: null,             // sensor value, i.e. distance in pixels; updated by sense() function
      },
      // define front-right proximity sensor
      {
        sense: senseDistance,    // function handle, determines type of sensor
        minVal: 0,               // minimum detectable distance, in pixels
        maxVal: simInfo.maxValProximitySensor, // maximum detectable distance, in pixels
        attachAngle: Math.PI/4,  // where the sensor is mounted on robot body
        lookAngle: 0,            // direction the sensor is looking (relative to center-out)
        id: "proximityFrontRight",    // a unique, arbitrary ID of the sensor, for printing/debugging
        parent: null,            // robot object the sensor is attached to, added by InstantiateRobot
        value: null,             // sensor value, i.e. distance in pixels; updated by sense() function
      },
      // define right proximity sensor
      // {
      //   sense: senseDistance,    // function handle, determines type of sensor
      //   minVal: 0,               // minimum detectable distance, in pixels
      //   maxVal: simInfo.maxValProximitySensor, // maximum detectable distance, in pixels
      //   attachAngle: Math.PI/2,  // where the sensor is mounted on robot body
      //   lookAngle: 0,            // direction the sensor is looking (relative to center-out)
      //   id: "proximityRight",    // a unique, arbitrary ID of the sensor, for printing/debugging
      //   parent: null,            // robot object the sensor is attached to, added by InstantiateRobot
      //   value: null,             // sensor value, i.e. distance in pixels; updated by sense() function
      // },
      // define left collision sensor
      // {
      //   sense: senseCollision,   // function handle, determines type of sensor
      //   minVal: 0,               // minimum detectable distance, in pixels
      //   maxVal: simInfo.maxValCollisionSensor, // maximum detectable distance, in pixels
      //   attachAngle: -Math.PI/2, // where the sensor is mounted on robot body
      //   lookAngle: 0,            // direction the sensor is looking (relative to center-out)
      //   id: "collisionLeft",     // a unique, arbitrary ID of the sensor, for printing/debugging
      //   parent: null,            // robot object the sensor is attached to, added by InstantiateRobot
      //   value: null,             // sensor value, boolean
      // },
      // define front-left collision sensor
      {
        sense: senseCollision,   // function handle, determines type of sensor
        minVal: 0,               // minimum detectable distance, in pixels
        maxVal: simInfo.maxValCollisionSensor, // maximum detectable distance, in pixels
        attachAngle: -Math.PI/4, // where the sensor is mounted on robot body
        lookAngle: 0,            // direction the sensor is looking (relative to center-out)
        id: "collisionFrontLeft",     // a unique, arbitrary ID of the sensor, for printing/debugging
        parent: null,            // robot object the sensor is attached to, added by InstantiateRobot
        value: null,             // sensor value, boolean
      },
      // define front-right collision sensor
      {
        sense: senseCollision,   // function handle, determines type of sensor
        minVal: 0,               // minimum detectable distance, in pixels
        maxVal: simInfo.maxValCollisionSensor, // maximum detectable distance, in pixels
        attachAngle: Math.PI/4,  // where the sensor is mounted on robot body
        lookAngle: 0,            // direction the sensor is looking (relative to center-out)
        id: "collisionFrontRight",    // a unique, arbitrary ID of the sensor, for printing/debugging
        parent: null,            // robot object the sensor is attached to, added by InstantiateRobot
        value: null,             // sensor value, boolean
      },
      // define front-right collision sensor
      // {
      //   sense: senseCollision,   // function handle, determines type of sensor
      //   minVal: 0,               // minimum detectable distance, in pixels
      //   maxVal: simInfo.maxValCollisionSensor, // maximum detectable distance, in pixels
      //   attachAngle: Math.PI/2,  // where the sensor is mounted on robot body
      //   lookAngle: 0,            // direction the sensor is looking (relative to center-out)
      //   id: "collisionRight",    // a unique, arbitrary ID of the sensor, for printing/debugging
      //   parent: null,            // robot object the sensor is attached to, added by InstantiateRobot
      //   value: null,             // sensor value, boolean
      // },
    ],
    neural_network: {
      proximity_layer: null, // array of nodes
      collision_layer: null, // array of nodes
      motor_layer:     null, // array of nodes
      connections_proximity_collision: null, // array of connections (tuples of indeces to node layers and weight?)
      connections_collision_motor:     null, // array of connections
    },
  },
];

robots = new Array();
sensors = new Array();

function init() {  // called once when loading HTML file
  const robotBay = document.getElementById("bay"),
        arena = document.getElementById("arena"),
        height = arena.height,
        width = arena.width;
  simInfo.height = height;
  simInfo.width = width;

  /* Create a MatterJS engine and world. */
  simInfo.engine = Matter.Engine.create();
  simInfo.world = simInfo.engine.world;
  simInfo.world.gravity.y = simInfo.gravity;
  simInfo.engine.timing.timeScale = 1;

  /* Create walls and boxes, and add them to the world. */
  // note that "roles" are custom properties for rendering (not from MatterJS)
  function getWall(x, y, width, height) {
    return Matter.Bodies.rectangle(x, y, width, height,
                                   {isStatic: true, role: "wall"});
  };
  const wall_lo = getWall(width/2, height-5, width-5, 5),
        wall_hi = getWall(width/2, 5, width-5, 5),
        wall_le = getWall(5, height/2, 5, height-15),
        wall_ri = getWall(width-5, height/2, 5, height-15);
  Matter.World.add(simInfo.world, [wall_lo, wall_hi, wall_le, wall_ri]);

  /* Add a bunch of boxes in a neat grid. */
  function getBox(x, y) {
    return Matter.Bodies.rectangle(x, y, simInfo.boxSize, simInfo.boxSize,
                                   {frictionAir: simInfo.airDrag,
                                    friction: simInfo.boxFric,
                                    mass: simInfo.boxMass,
                                    role: "box"});
  };
  const nBoxX = 4, nBoxY = 4,
        gapX = gapY = 75;
  const startX = (simInfo.width - (simInfo.boxSize * nBoxX) - (gapX * (nBoxX-1))) / 2;
  const startY = (simInfo.width - (simInfo.boxSize * nBoxY) - (gapY * (nBoxY-1))) / 2;
  const stack = Matter.Composites.stack(startX, startY,
                                        nBoxX, nBoxY,
                                        gapX, gapY, getBox);
  Matter.World.add(simInfo.world, stack);

  /* Add debugging mouse control for dragging objects. */
  if (simInfo.debugMouse){
    const mouseConstraint = Matter.MouseConstraint.create(simInfo.engine,
                              {mouse: Matter.Mouse.create(arena),
                               // spring stiffness mouse ~ object
                               constraint: {stiffness: 0.5}});
    Matter.World.add(simInfo.world, mouseConstraint);
  }
  // Add the tracker functions from mouse.js
  addMouseTracker(arena);
  addMouseTracker(robotBay);

  /* Running the MatterJS physics engine (without rendering). */
  simInfo.runner = Matter.Runner.create({fps: 60,  // TODO: why weird effects?
                                         isFixed: false});
  Matter.Runner.start(simInfo.runner, simInfo.engine);
  // register function simStep() as callback to MatterJS's engine events
  Matter.Events.on(simInfo.engine, "tick", simStep);

  /* Create robot(s). */
  setRobotNumber(1);  // requires defined simInfo.world
  loadBay(robots[0]);

  /* print the used (interesting parameters to the screen */
  let parameterString = "";
  parameterString += "<tr><td>maxSteps:</td><td>" + simInfo.maxSteps + "</td></tr>";
  parameterString += "<tr><td>airDrag:</td><td>" + simInfo.airDrag + "</td></tr>";
  parameterString += "<tr><td>boxFric:</td><td>" + simInfo.boxFric + "</td></tr>";
  parameterString += "<tr><td>boxMass:</td><td>" + simInfo.boxMass + "</td></tr>";
  parameterString += "<tr><td>boxSize:</td><td>" + simInfo.boxSize + "</td></tr>";
  parameterString += "<tr><td>robotSize:</td><td>" + simInfo.robotSize + "</td></tr>";
  parameterString += "<tr><td>robotMass:</td><td>" + simInfo.robotMass + "</td></tr>";
  parameterString += "<tr><td>gravity:</td><td>" + simInfo.gravity + "</td></tr>";
  parameterString += "<tr><td>bayScale:</td><td>" + simInfo.bayScale + "</td></tr>";
  parameterString += "<tr><td>doContinue:</td><td>" + simInfo.doContinue + "</td></tr>";
  parameterString += "<tr><td>debugSensors:</td><td>" + simInfo.debugSensors + "</td></tr>";
  parameterString += "<tr><td>debugMouse:</td><td>" + simInfo.debugMouse + "</td></tr>";
  parameterString += "<tr><td>height:</td><td>" + simInfo.height + "</td></tr>";
  parameterString += "<tr><td>width:</td><td>" + simInfo.width + "</td></tr>";
  // custom values
  parameterString += "<tr><td colspan=\"2\"><b>custom values:</b></td></tr>";
  parameterString += "<tr><td>defaultForce:</td><td>" + simInfo.defaultForce + "</td></tr>";
  parameterString += "<tr><td>maxTorque:</td><td>" + simInfo.maxTorque + "</td></tr>";
  parameterString += "<tr><td>learningRate:</td><td>" + simInfo.learningRate + "</td></tr>";
  parameterString += "<tr><td>forgettingRate:</td><td>" + simInfo.forgettingRate + "</td></tr>";
  parameterString += "<tr><td>activation_threshold:</td><td>" + simInfo.activation_threshold + "</td></tr>";
  parameterString += "<tr><td>maxWeightInit:</td><td>" + simInfo.maxWeightInit + "</td></tr>";
  parameterString += "<tr><td>maxValProximitySensor:</td><td>" + simInfo.maxValProximitySensor + "</td></tr>";
  parameterString += "<tr><td>maxValCollisionSensor:</td><td>" + simInfo.maxValCollisionSensor + "</td></tr>";
  parameterString += "<tr><td>maxClusterDistance:</td><td>" + simInfo.maxClusterDistance + "</td></tr>";

  document.getElementById("ParameterTable").innerHTML = parameterString;
};

function rotate(robot, torque=0) {
  /* Apply a torque to the robot to rotate it.
   *
   * Parameters
   *   torque - rotational force to apply to the body.
   */
  robot.body.torque = torque;
 };

function drive(robot, force=0) {
  /* Apply a force to the robot to move it.
   *
   * Parameters
   *   force - force to apply to the body.
   */
  const orientation = robot.body.angle,
        force_vec = Matter.Vector.create(force, 0),
        move_vec = Matter.Vector.rotate(force_vec, orientation);
  Matter.Body.applyForce(robot.body, robot.body.position , move_vec);
};


function senseCollision() {
  senseDistance.apply(this);
  this.value = isFinite(this.value) && this.value > 0 ? 1 : 0;
}

function senseDistance() {
  /* Distance sensor simulation based on ray casting. Called from sensor
   * object, returns nothing, updates a new reading into this.value.
   *
   * Idea: Cast a ray with a certain length from the sensor, and check
   *       via collision detection if objects intersect with the ray.
   *       To determine distance, run a Binary search on ray length.
   * Note: Sensor ray needs to ignore robot (parts), or start outside of it.
   *       The latter is easy with the current circular shape of the robots.
   * Note: Order of tests are optimized by starting with max ray length, and
   *       then only testing the maximal number of initially resulting objects.
   * Note: The sensor's "ray" could have any other (convex) shape;
   *       currently it's just a very thin rectangle.
   */

  const context = document.getElementById("arena").getContext("2d");
  let bodies = Matter.Composite.allBodies(simInfo.engine.world);

  const robotAngle = this.parent.body.angle,
        attachAngle = this.attachAngle,
        rayAngle = robotAngle + attachAngle + this.lookAngle;

  const rPos = this.parent.body.position,
        rSize = simInfo.robotSize,
        startPoint = {x: rPos.x + (rSize+1) * Math.cos(robotAngle + attachAngle),
                      y: rPos.y + (rSize+1) * Math.sin(robotAngle + attachAngle)};

  function getEndpoint(rayLength) {
    return {x: startPoint.x + rayLength * Math.cos(rayAngle),
            y: startPoint.y + rayLength * Math.sin(rayAngle)};
  };

  function sensorRay(bodies, rayLength) {
    // Cast ray of supplied length and return the bodies that collide with it.
    const rayWidth = 1e-100,
          endPoint = getEndpoint(rayLength);
    rayX = (endPoint.x + startPoint.x) / 2,
    rayY = (endPoint.y + startPoint.y) / 2,
    rayRect = Matter.Bodies.rectangle(rayX, rayY, rayLength, rayWidth,
                                      {isSensor: true, isStatic: true,
                                       angle: rayAngle, role: "sensor"});

    let collidedBodies = [];
    for (let bb = 0; bb < bodies.length; bb++) {
      let body = bodies[bb];
      // coarse check on body boundaries, to increase performance:
      if (Matter.Bounds.overlaps(body.bounds, rayRect.bounds)) {
        for (let pp = body.parts.length === 1 ? 0 : 1; pp < body.parts.length; pp++) {
          let part = body.parts[pp];
          // finer, more costly check on actual geometry:
          if (Matter.Bounds.overlaps(part.bounds, rayRect.bounds)) {
            const collision = Matter.SAT.collides(part, rayRect);
            if (collision.collided) {
              collidedBodies.push(body);
              break;
            }
          }
        }
      }
    }
    return collidedBodies;
  };

  // call 1x with full length, and check all bodies in the world;
  // in subsequent calls, only check the bodies resulting here
  let rayLength = this.maxVal;
  bodies = sensorRay(bodies, rayLength);

  // if some collided, search for maximal ray length without collisions
  if (bodies.length > 0) {
    let lo = 0,
        hi = rayLength;
    while (lo < rayLength) {
      if (sensorRay(bodies, rayLength).length > 0) {
        hi = rayLength;
      }
      else {
        lo = rayLength;
      }
      rayLength = Math.floor(lo + (hi-lo)/2);
    }
  }
  // increase length to (barely) touch closest body (if any)
  rayLength += 1;
  bodies = sensorRay(bodies, rayLength);

  if (simInfo.debugSensors) {  // if invisible, check order of object drawing
    // draw the resulting ray
    endPoint = getEndpoint(rayLength);
    context.beginPath();
    context.moveTo(startPoint.x, startPoint.y);
    context.lineTo(endPoint.x, endPoint.y);
    context.strokeStyle = this.parent.info.color;
    context.lineWidth = 0.5;
    context.stroke();
    // mark all objects's lines intersecting with the ray
    for (let bb = 0; bb < bodies.length; bb++) {
      let vertices = bodies[bb].vertices;
      context.moveTo(vertices[0].x, vertices[0].y);
      for (let vv = 1; vv < vertices.length; vv += 1) {
        context.lineTo(vertices[vv].x, vertices[vv].y);
      }
      context.closePath();
    }
    context.stroke();
  }

  // indicate if the sensor exceeded its maximum length by returning infinity
  if (rayLength > this.maxVal) {
    rayLength = Infinity;
  }
  else {
    // apply mild noise on the sensor reading, and clamp between valid values
    function gaussNoise(sigma=1) {
      const x0 = 1.0 - Math.random();
      const x1 = 1.0 - Math.random();
      return sigma * Math.sqrt(-2 * Math.log(x0)) * Math.cos(2 * Math.PI * x1);
    };
    rayLength = Math.floor(rayLength + gaussNoise(3));
    rayLength = Matter.Common.clamp(rayLength, this.minVal, this.maxVal);
  }

  this.value = rayLength;
};


function dragSensor(sensor, event) {
  const robotBay = document.getElementById("bay"),
        bCenter = {x: robotBay.width/2,
                   y: robotBay.height/2},
        rSize = simInfo.robotSize,
        bScale = simInfo.bayScale,
        sSize = sensor.getWidth(),
        mAngle = Math.atan2(  event.mouse.x - bCenter.x,
                            -(event.mouse.y - bCenter.y));
  sensor.info.attachAngle = mAngle;
  sensor.x = bCenter.x - sSize - bScale * rSize * Math.sin(-mAngle);
  sensor.y = bCenter.y - sSize - bScale * rSize * Math.cos( mAngle);
  repaintBay();
}

function loadSensor(sensor, event) {
  loadSensorInfo(sensor.sensor);
}

function loadSensorInfo(sensorInfo) {
  simInfo.baySensor = sensorInfo;
}

function loadBay(robot) {
  simInfo.bayRobot = robot;
  sensors = new Array();
  const robotBay = document.getElementById("bay");
  const bCenter = {x: robotBay.width/2,
                   y: robotBay.height/2},
        rSize = simInfo.robotSize,
        bScale = simInfo.bayScale;

  for (let ss = 0; ss < robot.info.sensors.length; ++ss) {
    const curSensor = robot.sensors[ss],
          attachAngle = curSensor.attachAngle;
    // put current sensor into global letiable, make mouse-interactive
    sensors[ss] = makeInteractiveElement(new SensorGraphics(curSensor),
                                         document.getElementById("bay"));
    const sSize = sensors[ss].getWidth();
    sensors[ss].x = bCenter.x - sSize - bScale * rSize * Math.sin(-attachAngle);
    sensors[ss].y = bCenter.y - sSize - bScale * rSize * Math.cos( attachAngle);
    sensors[ss].onDragging = dragSensor;
    sensors[ss].onDrag = loadSensor;
  }
  repaintBay();
}

function SensorGraphics(sensorInfo) {
  this.info = sensorInfo;
  this.plotSensor = plotSensor;
  // add functions getWidth/getHeight for graphics.js & mouse.js,
  // to enable dragging the sensor in the robot bay
  this.getWidth = function() { return 6; };
  this.getHeight = function() { return 6; };
}

function modulo(m, n) { return ((m % n) + n) % n; }

function InstantiateRobot(robotInfo) {
  // create robot's main physical body (simulated with MatterJS engine)
  const nSides = 20,
        circle = Matter.Bodies.circle;
  this.body = circle(robotInfo.init.x, robotInfo.init.y, simInfo.robotSize,
                     {frictionAir: simInfo.airDrag,
                       mass: simInfo.robotMass,
                       role: "robot"}, nSides);
  Matter.World.add(simInfo.world, this.body);
  Matter.Body.setAngle(this.body, robotInfo.init.angle);

  // instantiate its sensors
  this.sensors = robotInfo.sensors;
  for (let ss = 0; ss < this.sensors.length; ++ss) {
    this.sensors[ss].parent = this;
  }

  // ==============================
  // instantiate the neural network
  // ==============================
  this.neural_network = robotInfo.neural_network;

  // create proximity and collision layer
  this.neural_network.proximity_layer = new Array();
  this.neural_network.collision_layer = new Array();
  for (let ss = 0; ss < this.sensors.length; ++ss) {
    if (this.sensors[ss].sense == senseDistance) {
      // proximity sensor node: sensor index, activation level
      this.neural_network.proximity_layer.push({si: ss, a: 0});
    } else if (this.sensors[ss].sense == senseCollision) {
      // collision sensor node: sensor index, activation level
      this.neural_network.collision_layer.push({si: ss, a: 0});
    }
  }

  // create motor layer
  this.neural_network.motor_layer = new Array({a: 0}, {a: 0}); // left right

  // create connections between proximity and collision layers
  this.neural_network.connections_proximity_collision = new Array();
  for (let cni = 0; cni < this.neural_network.collision_layer.length; ++cni) {
    this.neural_network.connections_proximity_collision[cni] = new Array();
    for (let pni = 0; pni < this.neural_network.proximity_layer.length; ++pni) {
      let initial_w = Math.random() * simInfo.maxWeightInit;
      this.neural_network.connections_proximity_collision[cni][pni] = initial_w;
    }
  }

  // create connections between collision and motor layers
  this.neural_network.connections_collision_motor = new Array();
  for (let cni = 0; cni < this.neural_network.collision_layer.length; ++cni) {
    // check if this collision sensor is left or right on the body
    let sensor = this.sensors[this.neural_network.collision_layer[cni].si];
    let attachAngle = modulo(sensor.attachAngle, 2 * Math.PI);
    // connection collision layer -> motor layer:
    // collision node index, motor node index
    if (attachAngle >= Math.PI) {
      // left motor
      this.neural_network.connections_collision_motor.push({cni: cni, mni: 0, w: 1});
    } else {
      // right motor
      this.neural_network.connections_collision_motor.push({cni: cni, mni: 1, w: 1});
    }
  }

  // ==============================

  // attach its helper functions
  this.rotate = rotate;
  this.drive = drive;
  this.info = robotInfo;
  this.plotRobot = plotRobot;

  // add functions getWidth/getHeight for graphics.js & mouse.js,
  // to enable selection by clicking the robot in the arena
  this.getWidth = function() { return 2 * simInfo.robotSize; };
  this.getHeight = function() { return 2 * simInfo.robotSize; };
}

function getNormalisedValue(sensor) {
  // get distance from sensor (no detection is same as max detectable
  // distance
  var distance = isFinite(sensor.value) ? sensor.value : sensor.maxVal;
  // convert value to 0-1.0 (0.0 is up close, 1.0 is max
  // distance/no detection)
  var normalised = (distance - sensor.minVal) / (sensor.maxVal - sensor.minVal);
  // invert value to get more sensible range (1.0 is up close 0.0 is max
  // distance/no detection)
  console.assert(0 <= normalised && normalised <= 1);
  return 1.0 - normalised;
}


function activation_function_linear(h) {
  return h;
}

function activation_function_linear_threshold(h, theta) {
  return h >= theta ? 1 : 0;
}

function activation_function_sigmoid(h) {
  // Normalized tunable sigmoid functions
  // https://dinodini.wordpress.com/2010/04/05/normalized-tunable-sigmoid-functions/
  // https://www.wolframalpha.com/input/?i=Plot%5BReplaceAll%5B(k+x)%2F(k+-+x+%2B+1),+%7Bk+-%3E+-1.2%7D%5D,+%7Bx,+0,+1%7D%5D
  console.assert(-1 <= h && h <= 1);

  let k = -1.4; // TODO make parameter
  let sign = Math.sign(h); // For completion, we don't actually use this for negative values

  let x = Math.abs(h);
  let P = (k * x) / (k - x + 1);

  return sign * P;
}

function robotUpdateNeuralNetwork(robot) {
  // update the neural network of the robot, calculate weights and calculate values output of nodes


  // calculate new weights of the proximity layer -> collision layer connections
  // store them seperate to user later.
  let collision_activation_sum = 0;
  for (let cni = 0; cni < robot.neural_network.collision_layer.length; cni++) {
    collision_activation_sum += robot.neural_network.collision_layer[cni].a;
  }
  let average_activation = collision_activation_sum / robot.neural_network.collision_layer.length;
  let N = robot.neural_network.proximity_layer.length;
  let new_weights = [...Array(robot.neural_network.collision_layer.length).keys()].map(i => Array(N));
  for (let cni = 0; cni < robot.neural_network.collision_layer.length; cni++) {
    for (let pni = 0; pni < N; pni++) {
      let curr_w = robot.neural_network.connections_proximity_collision[cni][pni];
      let learn  = simInfo.learningRate * robot.neural_network.proximity_layer[pni].a * robot.neural_network.collision_layer[cni].a;
      let forget = simInfo.forgettingRate * average_activation * curr_w;
      let delta_w = (learn - forget) / N;
      new_weights[cni][pni] = robot.neural_network.connections_proximity_collision[cni][pni] + delta_w;
    }
  }

  // determine activation levels of motor layer nodes
  for (let mni = 0; mni < robot.neural_network.motor_layer.length; mni++) {
    let summed_input = 0;
    for (let con_i = 0; con_i < robot.neural_network.connections_collision_motor.length; con_i++) {
      let con = robot.neural_network.connections_collision_motor[con_i];
      if (con.mni == mni) {
        summed_input += con.w * robot.neural_network.collision_layer[con.cni].a;
      }
    }
    let activation = activation_function_linear_threshold(summed_input, 0.5); // FIXME magic nr
    robot.neural_network.motor_layer[mni].a = activation;
  }

  // determine activation levels of collision layer nodes
  for (let cni = 0; cni < robot.neural_network.collision_layer.length; cni++) {
    let summed_input = 0;
    for (let pni = 0; pni < robot.neural_network.proximity_layer.length; pni++) {
      let w = robot.neural_network.connections_proximity_collision[cni][pni];
      summed_input += w * robot.neural_network.proximity_layer[pni].a;
    }
    let sensor_value = robot.sensors[robot.neural_network.collision_layer[cni].si].value;
    summed_input += sensor_value;
    let activation = activation_function_linear_threshold(summed_input, simInfo.activation_threshold);
    robot.neural_network.collision_layer[cni].a = activation;
  }

  // determine activation/output levels of proximity layer nodes
  for (let pni = 0; pni < robot.neural_network.proximity_layer.length; pni++) {
    let activation = getNormalisedValue(robot.sensors[robot.neural_network.proximity_layer[pni].si]);
    robot.neural_network.proximity_layer[pni].a = activation_function_sigmoid(activation);
  }

  // swap old weights for new weights
  robot.neural_network.connections_proximity_collision = new_weights;
};

function robotUpdateSensors(robot) {
  // update all sensors of robot; puts new values into sensor.value
  for (let ss = 0; ss < robot.sensors.length; ss++) {
    robot.sensors[ss].sense();
  }
};

function getSensorValById(robot, id) {
  for (let ss = 0; ss < robot.sensors.length; ss++) {
    if (robot.sensors[ss].id == id) {
      return robot.sensors[ss].value;
    }
  }
  return undefined;  // if not returned yet, id doesn"t exist
};

function robotMove(robot) {
  // This function is called each timestep and should be used to move the robots

  // move based on neural network outputs
  let activation_left  = robot.neural_network.motor_layer[0].a > 0;
  let activation_right = robot.neural_network.motor_layer[1].a > 0;

  let force  = 0;
  let torque = 0;
  if (!activation_left && !activation_right) {
    // no collision, drive forward
    force = simInfo.defaultForce;
  } else if (activation_left && !activation_right) {
    // collision left, turn right
    torque = +simInfo.maxTorque;
    force = simInfo.defaultForce/10;
  } else if (!activation_left && activation_right) {
    // collision right, turn right
    torque = -simInfo.maxTorque;
    force = simInfo.defaultForce/10;
  } else if (activation_left && activation_right) {
    // collision both sides
    // turn away in a random direction

    // chosen by fair dice roll.
    // garantueed to be random.
    torque = -simInfo.maxTorque;
    force = simInfo.defaultForce/10;
  } else {
    // Ce n'est pas possible
    console.assert(false);
  }

  robot.drive(robot, force);
  robot.rotate(robot, torque);
};

function plotSensor(context, x = this.x, y = this.y) {
  context.beginPath();
  context.arc(x + this.getWidth()/2,
              y + this.getHeight()/2,
              this.getWidth()/2, 0, 2*Math.PI);
  context.closePath();
  context.fillStyle = "black";
  context.strokeStyle = "black";
  context.fill();
  context.stroke();
}

function plotRobot(context,
                     xTopLeft = this.body.position.x,
                     yTopLeft = this.body.position.y) {
  let x, y, scale, angle, i, half, full,
      rSize = simInfo.robotSize;

  if (context.canvas.id == "bay") {
    scale = simInfo.bayScale;
    half = Math.floor(rSize/2*scale);
    full = half * 2;
    x = xTopLeft + full;
    y = yTopLeft + full;
    angle = -Math.PI / 2;
  } else {
    scale = 1;
    half = Math.floor(rSize/2*scale);
    full = half * 2;
    x = xTopLeft;
    y = yTopLeft;
    angle = this.body.angle;
  }
  context.save();
  context.translate(x, y);
  context.rotate(angle);

  // Plot wheels as rectangles.
  context.strokeStyle = "black";
  if (context.canvas.id == "bay") {
    context.fillStyle = "grey";
    context.fillRect(-half, -full, full, full);
    context.fillRect(-half, 0, full, full);
  } else {
    context.fillStyle = "grey";
    context.fillRect(-half, -full, full, 2*full);
  }
  context.strokeRect(-half, -full, full, 2*full);

  // Plot circular base object.
  if (context.canvas.id == "bay") {
    context.beginPath();
    context.arc(0, 0, full, 0, 2*Math.PI);
    context.closePath();
    context.fillStyle = "lightgrey";
    context.fill();
    context.stroke();
  }
  else { // context.canvas.id == "arena"
    // draw into world canvas without transformations,
    // because MatterJS thinks in world coords...
    context.restore();
    context.beginPath();
    let vertices = this.body.vertices;
    context.moveTo(vertices[0].x, vertices[0].y);
    for (let vv = 1; vv < vertices.length; vv += 1) {
      context.lineTo(vertices[vv].x, vertices[vv].y);
    }
    context.closePath();
    context.fillStyle = "lightgrey";
    context.stroke();
    context.fill();
    // to draw the rest, rotate & translate again
    context.save();
    context.translate(x, y);
    context.rotate(angle);
  }

  // Plot a marker to distinguish robots and their orientation.
  context.beginPath();
  context.arc(0, 0, full * .4, -Math.PI/4, Math.PI/4);
  context.lineTo(full * Math.cos(Math.PI/4) * .8,
             full * Math.sin(Math.PI/4) * .8);
  context.arc(0, 0, full * .8, Math.PI/4, -Math.PI/4, true);
  context.closePath();
  context.fillStyle = this.info.color;
  context.fill();
  context.stroke();

  // Plot sensor positions into world canvas.
  if (context.canvas.id == "arena") {
    for (ss = 0; ss < this.info.sensors.length; ++ss) {
      context.beginPath();
      context.arc(full * Math.cos(this.info.sensors[ss].attachAngle),
                  full * Math.sin(this.info.sensors[ss].attachAngle),
                  scale, 0, 2*Math.PI);
      context.closePath();
      context.fillStyle = "black";
      context.strokeStyle = "black";
      context.fill();
      context.stroke();
    }
  }
  context.restore();
}

function printWeights() {
  const nn = simInfo.bayRobot.neural_network;

  let string = "";
  for (let cni = 0; cni < nn.collision_layer.length; cni++) {
    for (let pni = 0; pni < nn.proximity_layer.length; pni++) {
      let w = nn.connections_proximity_collision[cni][pni];
      string += w + " "
    }
  }
  console.log("\"step " + simInfo.curSteps + "\" " + string);
}

function simStep() {
  if ((simInfo.curSteps % 1000) == 0) {
    printWeights();
  }
  repaintBay();
  drawBoard();

  // advance simulation by one step (except MatterJS engine's physics)
  if (simInfo.curSteps < simInfo.maxSteps) {
    for (let rr = 0; rr < robots.length; ++rr) {
      let robot = robots[rr];
      robotUpdateSensors(robot);
      robotUpdateNeuralNetwork(robot);
      repaintBay();
      robotMove(robot);
      // To enable selection by clicking (via mouse.js/graphics.js),
      // the position on the canvas needs to be defined in (x, y):
      robot.x = robot.body.position.x - simInfo.robotSize;
      robot.y = robot.body.position.y - simInfo.robotSize;
    }
    // count and display number of steps
    simInfo.curSteps += 1;
    document.getElementById("SimStepLabel").innerHTML =
      padnumber(simInfo.curSteps, 5) +
      " of " +
      padnumber(simInfo.maxSteps, 5);
  }
  else {
    toggleSimulation();
  }
}

function drawBoard() {
  let context = document.getElementById("arena").getContext("2d");
  context.fillStyle = "#444444";
  context.fillRect(0, 0, simInfo.width, simInfo.height);

  // draw objects within world
  const Composite = Matter.Composite,
        bodies = Composite.allBodies(simInfo.world);
  context.beginPath();
  for (let bb = 0; bb < bodies.length; bb += 1) {
    if (bodies[bb].role == "robot") {
      // don't draw robot's circles; they're drawn below
      continue;
    }
    else {
      let vertices = bodies[bb].vertices;
      context.moveTo(vertices[0].x, vertices[0].y);
      for (let vv = 1; vv < vertices.length; vv += 1) {
        context.lineTo(vertices[vv].x, vertices[vv].y);
      }
      context.closePath();
    }
  }
  context.lineWidth = 1;
  context.strokeStyle = "#999";
  context.fillStyle = "#5559";
  context.fill();
  context.stroke();

  // draw all robots
  for (let rr = 0; rr < robots.length; ++rr) {
    robots[rr].plotRobot(context);
  }
}

function grayTableData(val, colspan=1) {
  let colour = Math.round(val * 255);
  let content = Math.round(val * 100);
  let s = "<td ";
  s += "colspan = \"" + colspan + "\" ";

  s += "style=\"";
  s += "background-color: rgb(" + colour + ", " + colour + ", " + colour + "); ";
  s += "width:  15px; ";
  s += "height: 15px; ";
  s += "\""

  s += ">";

  colour = colour > 200 ? 0 : 255;
  s += "<span style=\"font-size: xx-small; color: rgb(" + colour + ", " + colour + ", " + colour + ");\">";
  s += content
  s += "</span>";

  s += "</td>";
  return s;
}

function repaintBay() {
  // update inset canvas showing information about selected robot
  const robotBay = document.getElementById("bay"),
        context = robotBay.getContext("2d");
  context.clearRect(0, 0, robotBay.width, robotBay.height);
  simInfo.bayRobot.plotRobot(context, 10, 10);
  for (let ss = 0; ss < sensors.length; ss++) {
    sensors[ss].plotSensor(context);
  }

  if (!(simInfo.curSteps % 5)) {  // update slow enough to read
    // print sensor values of selected robot next to canvas
    let sensorString = "";
    const rsensors = simInfo.bayRobot.sensors;
    for (let ss = 0; ss < rsensors.length; ss++) {
      sensorString += "<tr><td>" + rsensors[ss].id + "</td>" + "<td>";
      sensorString += rsensors[ss].value + "</td></tr>";
    }
    document.getElementById("SensorLabel").innerHTML = sensorString;

    // print neural network values
    let neuralNetworkString = "";
    const nn = simInfo.bayRobot.neural_network;
    // motor node layer
    neuralNetworkString += "<tr>";
    neuralNetworkString += "<td><b>m:</b></td>";
    for (let n = 0; n < nn.motor_layer.length; ++n) {
      let a = nn.motor_layer[n].a;
      neuralNetworkString += grayTableData(a);
    }
    neuralNetworkString += "</tr>";
    // collision node layer
    neuralNetworkString += "<tr>";
    neuralNetworkString += "<td><b>c:</b></td>";
    for (let n = 0; n < nn.collision_layer.length; ++n) {
      let a = nn.collision_layer[n].a;
      neuralNetworkString += grayTableData(a);
    }
    neuralNetworkString += "</tr>";

    // connection weights
    neuralNetworkString += "<td><b>w:</b></td>";
    neuralNetworkString += "<td colspan = \"" + nn.proximity_layer.length + "\">";
    neuralNetworkString += "<table style=\"width: 100%\">";
    for (let cni = 0; cni < nn.collision_layer.length; cni++) {
      neuralNetworkString += "<tr>";
      for (let pni = 0; pni < nn.proximity_layer.length; pni++) {
        let w = nn.connections_proximity_collision[cni][pni];
        neuralNetworkString += grayTableData(w);
      }
      neuralNetworkString += "</tr>";
    }
    neuralNetworkString += "</table></td>";
    neuralNetworkString += "</tr>";

    // proximity node layer
    neuralNetworkString += "<tr>";
    neuralNetworkString += "<td><b>p:</b></td>";
    for (let n = 0; n < nn.proximity_layer.length; ++n) {
      let a = nn.proximity_layer[n].a;
      neuralNetworkString += grayTableData(a);
    }
    neuralNetworkString += "</tr>";

    document.getElementById("NeuralNetworkLabel").innerHTML = neuralNetworkString;
  }


}

function setRobotNumber(newValue) {
  let n;
  while (robots.length > newValue) {
    n = robots.length - 1;
    Matter.World.remove(simInfo.world, robots[n].body);
    robots[n] = null;
    robots.length = n;
  }

  while (robots.length < newValue) {
    if (newValue > RobotInfo.length) {
      console.warn("You request "+newValue+" robots, but only " + RobotInfo.length +
                   " are defined in RobotInfo!");
      toggleSimulation();
      return;
    }
    n = robots.length;
    robots[n] = makeInteractiveElement(new InstantiateRobot(RobotInfo[n]),
                                       document.getElementById("arena"));

    robots[n].onDrop = function(robot, event) {
      robot.isDragged = false;
    };

    robots[n].onDrag = function(robot, event) {
        robot.isDragged = true;
        loadBay(robot);
        return true;
    };
  }
}

function padnumber(number, size) {
  if (number == Infinity) {
    return "inf";
  }
  const s = "000000" + number;
  return s.substr(s.length - size);
}

function format(number) {
  // prevent HTML elements to jump around at sign flips etc
  return (number >= 0 ? "+" : "−") + Math.abs(number).toFixed(1);
}

function distance_points(a, b) {
  return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
}

function distance_point_line_segment(p, a, b) {
  // distance between a point (p), and a line segment (a, b)
  // http://paulbourke.net/geometry/pointlineplane/

  // calculate tangent intersection point

  let mag_sq = Matter.Vector.magnitudeSquared(Matter.Vector.sub(b, a));
  console.assert(mag_sq != 0);
  if (mag_sq == 0) {
    // line segment is actually a point (a = b)
    return distance_points(a, proj);
  }

  // calculate u and clamp on [0, 1] so the projection always falls on the
  // line segment.
  let u = ((p.x - a.x)*(b.x - a.x) + (p.y - a.y)*(b.y - a.y)) / mag_sq
  u = Math.max(0, Math.min(1, u));

  // return distance between projection and point.
  let proj_x = a.x + u * (b.x - a.x)
  let proj_y = a.y + u * (b.y - a.y)
  let proj = Matter.Vector.create(proj_x, proj_y);

  return distance_points(p, proj);
}

function distance_line_segments(a, b, c, d) {
  // distance between two line segments, AB and DC.
  // both are defined by their endpoints (a, b), and (c, d).

  // create array with distances between endpoints of each line segment and
  // the other line segment.
  let dis_array = Array();
  dis_array.push(distance_point_line_segment(a, c, d));
  dis_array.push(distance_point_line_segment(b, c, d));
  dis_array.push(distance_point_line_segment(c, a, b));
  dis_array.push(distance_point_line_segment(d, a, b));
  // and return the minimum distance
  return Math.min.apply(null, dis_array);
}

function distance_boxes(boxes, i, j) {
  // distance between the boxes at index i and j
  let i_nr_vertices = boxes[i].vertices.length;
  let j_nr_vertices = boxes[j].vertices.length;

  // create array with distances between all line segments of box i and box j
  let dis_array = Array();
  for (let vi = 0; vi < i_nr_vertices; ++vi) {
    let a = boxes[i].vertices[vi];
    let b = boxes[i].vertices[(vi+1) % i_nr_vertices];

    for (let vj = 0; vj < j_nr_vertices; ++vj) {
      let c = boxes[j].vertices[vj];
      let d = boxes[j].vertices[(vj+1) % j_nr_vertices];

      let dis = distance_line_segments(a, b, c, d);
      dis_array.push(dis);
    }
  }

  // and return the minimum distance
  return Math.min.apply(null, dis_array);
}

function count_clusters(max_distance) {
  let boxes = Matter.Composite.allBodies(simInfo.engine.world)
    .filter(body => body.role == "box");

  // create array to store if already put a box in a cluster
  let seen = Array(boxes.length);
  seen.fill(false);

  function dfs(i) {
    // check if element i is part of a cluster, and if so return the cluster
    // if it was already part of a cluster return null.
    // uses recursive depth first search
    if (seen[i]) {
      return null;
    }

    let cluster = Array();
    cluster.push(i);
    seen[i] = true;
    for (let j = 0; j < boxes.length; ++j) {
      let dist = distance_boxes(boxes, i, j);
      if (dist && dist <= max_distance) {
        let rest = dfs(j);
        if (rest != null) {
          cluster = cluster.concat(rest);
        }
      }
    }
    return cluster;
  }

  // go over all boxes, generating the clusters
  let clusters = Array();
  for (let i = 0; i < boxes.length; ++i) {
    let cluster = dfs(i);
    if (cluster != null) {
      cluster.sort((a, b) => a - b);
      clusters.push(cluster);
    }
  }

  return clusters;
}


function toggleSimulation() {
  simInfo.doContinue = !simInfo.doContinue;
  if (simInfo.doContinue) {
    Matter.Runner.start(simInfo.runner, simInfo.engine);
  }
  else {
    Matter.Runner.stop(simInfo.runner);
    console.log("Clusters:");

    let clusters = count_clusters(simInfo.maxClusterDistance);
    let nr_of_clusters = 0;
    let cluster_sizes = "";
    let blocks_in_cluster = 0;
    for (let i = 0; i < clusters.length; ++i) {
      let cluster = clusters[i];
      if (cluster.length > 1) {
        nr_of_clusters += 1;
        cluster_sizes += cluster.length + ", ";
        blocks_in_cluster += cluster.length;
      }
    }

    console.log(clusters.map(r => r.join(' ')).join('\n'));
    if (nr_of_clusters == 0) {
      console.log("nr. of clusters: " + nr_of_clusters);
    } else if (nr_of_clusters == 1) {
      console.log("nr. of clusters: " + nr_of_clusters + ", size: " + cluster_sizes.substr(0, cluster_sizes.length - 2));
    } else {
      console.log("nr. of clusters: " + nr_of_clusters + ", sizes: " + cluster_sizes.substr(0, cluster_sizes.length - 2));
    }
    console.log("% in clusters:  " + (blocks_in_cluster / 16) * 100 + "%");
  }
}
